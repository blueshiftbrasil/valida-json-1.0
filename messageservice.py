import falcon
import json
from jsonschema import validate, ValidationError

ALLOWED_TYPES = {
    'application/json': 'JSON'
    , 'application/javascript': 'JSON'
}


class MessageResource(object):

    def validateFormat(req, resp, resource, params):

        # Lê o arquivo JSON com o schema da mensagem que será validada.
        with open('send_schema.json') as data_file:
            jsonschema = json.load(data_file)

        # Recupera o JSON do POST HTTP.
        jsondata = req.stream.read()

        try:
            # Transfoma em objeto JSON.
            parsedjson = json.loads(jsondata)
            # Aplica a validação.
            validate(parsedjson, jsonschema)
        except (ValidationError, ValueError) as e:

            if isinstance(e, ValidationError):
                msg = e.message
                detail = e.instance
                expected = e.validator_value
            else:
                msg = str(e)
                detail = None
                expected = None

            d = {
                'success': False
                , 'code': 400
                , 'msg': 'Encontrado erro ao validar JSON. \n' + msg
                , 'detail': detail
                , 'expected': expected
            }
            # Caso a validação falhe, uma mensagem de 'Bad request' será lançada.
            raise falcon.HTTPBadRequest('Bad request', d)

    def validateDataType(req, resp, resource, params):
        if req.content_type not in ALLOWED_TYPES:
            resp.status = falcon.HTTP_400
            msg = 'Type not allowed. Must be XML or JSON'

            d = {
                'success': False
                , 'code': 400
                , 'msg': msg
            }
            # Caso a validação falhe, uma mensagem de 'Bad request' será lançada.
            raise falcon.HTTPBadRequest('Bad request', d)

    @falcon.before(validateDataType)
    @falcon.before(validateFormat)
    def on_post(self, req, resp):
        d = {
                'success': True
        }

        resp.status = falcon.HTTP_200
        resp.body = json.dumps(d, indent=False)
