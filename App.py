import falcon
import messageservice

# Instancia o Falcon Framework.
app = falcon.API()

# Instancia as classes que irao tratar os requests.
messageSend = messageservice.MessageResource()

# Crias as rotas para receber os POSTS HTTP.
app.add_route('/message', messageSend)