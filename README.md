# Pequeno tutorial para dominação do mundo (dos dados): Parte 1 #

Olá! Nesse pequeno tutorial iremos validar o schema de mensagens JSON (json-schema-validation) utilizando Python + Falcon framework + Gunicorn.

O Falcom é um framework extremamente simples e performático para criação de aplicações REST. Ele roda muito bem sobre Gunicorn, que é um Python WSGI HTTP Server para UNIX. Ops!!! Você vai precisar de um Linux (ou um Mac)! 
Para saber mais sobre Falcon framework: http://falconframework.org/
E sobre Gunicorn: http://gunicorn.org/

Pré requisitos para execução do tutorial:

um Linux ou Mac, por conta do Gunicorn.
Python 2.7
pip (https://pip.pypa.io/en/stable/)
um REST tester, eu estou usando um plug-in do Chrome chamado Postman, pois precisaremos enviar um POST HTTP para nosso "menino".
e por último, mas não menos importante: coragem!!!
Com todos os pré-requisitos em mãos, vamos ao tutorial...
Instalando dependências via pip:


```
#!shell

sudo pip install gunicorn
sudo pip install cython
sudo pip install falcon
sudo pip install jsonschema
```


Agora a execução! 

Para executar o aplicativo entre na pasta onde o mesmo foi descomprimido e execute o seguinte comando:


```
#!shell

gunicorn -w 5 -b 0.0.0.0:8000 App:app
```


O número de workers recomendados (passados para o paramêtro -w) é: (2 x $num_cores) + 1 . Para saber mais: http://docs.gunicorn.org/en/stable/design.html#how-many-workers

Após a instalação das dependências e "start" do aplicativo, podemos executar um teste. Ahh... já criei algumas mensagens para testes enquanto criava esse aplicativo, estão na pasta exemplo-msg na própria pasta do aplicativo.

Através do Postman envie algumas mensagens para a URL 127.0.0.1:8000. Não esqueça (ou esqueça, pois esse seria outro teste muito bom) de configurar o conteúdo da mensagem para application/json ou application/javascript, assim como na imagem abaixo:
![postman.png](https://bitbucket.org/repo/oX9RXb/images/2063634927-postman.png)

Voilà!!! Nosso menino está validando as mensagens JSON como gente grande.
Para saber mais sobre validação de esquemas JSON visite o site: http://json-schema.org/latest/json-schema-validation.html
E para mais exemplos: http://json-schema.org/example2.html

Bom, esse é só o começo!
No próximo post iremos utilizar nosso menino em conjunto com ferramentas de ingestão, tais como Apache Flume (http://flume.apache.org/) e Fluentd (http://www.fluentd.org/) para realizar validação de mensagens JSON em real time.

Até a próxima!